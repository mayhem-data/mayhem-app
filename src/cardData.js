/* eslint-disable no-template-curly-in-string */
const CardData = [
  {
    cardName: "For Justice!",
    characterName: "Lia The Radiant",
    class: "Paladin",
    countInDeck: 3,
    attack: 1,
    playAnotherCard: 1
  },
  {
    cardName: "Divine Shield",
    characterName: "Lia The Radiant",
    class: "Paladin",
    countInDeck: 2,
    sheild: 3
  },
  {
    cardName: "High Charisma",
    characterName: "Lia The Radiant",
    class: "Paladin",
    countInDeck: 2,
    draw: 2
  },
  {
    cardName: "Fighting Words",
    characterName: "Lia The Radiant",
    class: "Paladin",
    countInDeck: 3,
    attack: 2,
    heal: 1
  },
  {
    cardName: "Spinning Parry",
    characterName: "Lia The Radiant",
    class: "Paladin",
    countInDeck: 2,
    draw: 1,
    sheild: 1
  },
  {
    cardName: "Divine Smite",
    characterName: "Lia The Radiant",
    class: "Paladin",
    countInDeck: 3,
    attack: 3,
    heal: 1
  },
  {
    cardName: "Finger Wag of Judgement",
    characterName: "Lia The Radiant",
    class: "Paladin",
    countInDeck: 2,
    playAnotherCard: 2
  },
  {
    cardName: "Fluffy",
    characterName: "Lia The Radiant",
    class: "Paladin",
    countInDeck: 1,
    sheild: 2
  },
  {
    cardName: "For Even More Justice!",
    characterName: "Lia The Radiant",
    class: "Paladin",
    countInDeck: 4,
    attack: 2
  },
  {
    cardName: "For the Most Justice!",
    characterName: "Lia The Radiant",
    class: "Paladin",
    countInDeck: 2,
    attack: 3
  },
  {
    cardName: "Cure Wounds",
    characterName: "Lia The Radiant",
    class: "Paladin",
    countInDeck: 1,
    heal: 1,
    draw: 2
  },
  {
    cardName: "Divine Inspiration",
    supportingText:
      "Choose any card in your discard pile and put it into your hand, then ${healIcon} ${healIcon}.",
    characterName: "Lia The Radiant",
    class: "Paladin",
    countInDeck: 2,
    heal: 2,
    scry: 1
  },
  {
    cardName: "Banishing Smite",
    supportingText:
      "Destroy all ${sheildIcon} cards in play (including yours), then ${playAnotherCardIcon}.",
    characterName: "Lia The Radiant",
    class: "Paladin",
    countInDeck: 1,
    playAnotherCard: 1,
    globalShieldBreak: 1
  },
  {
    cardName: "Speed of Thought",
    characterName: "Azzan The Mystic",
    class: "Wizard",
    countInDeck: 3,
    playAnotherCard: 2
  },
  {
    cardName: "Stoneskin",
    characterName: "Azzan The Mystic",
    class: "Wizard",
    countInDeck: 1,
    sheild: 2
  },
  {
    cardName: "Knowledge is Power",
    characterName: "Azzan The Mystic",
    class: "Wizard",
    countInDeck: 3,
    draw: 3
  },
  {
    cardName: "Shield",
    characterName: "Azzan The Mystic",
    class: "Wizard",
    countInDeck: 2,
    draw: 1,
    sheild: 1
  },
  {
    cardName: "Lightning Bolt",
    characterName: "Azzan The Mystic",
    class: "Wizard",
    countInDeck: 4,
    attack: 3
  },
  {
    cardName: "Burning Hands",
    characterName: "Azzan The Mystic",
    class: "Wizard",
    countInDeck: 3,
    attack: 2
  },
  {
    cardName: "Evil Sneer",
    characterName: "Azzan The Mystic",
    class: "Wizard",
    countInDeck: 2,
    playAnotherCard: 1,
    heal: 1
  },
  {
    cardName: "Magic Missile",
    characterName: "Azzan The Mystic",
    class: "Wizard",
    countInDeck: 3,
    attack: 1,
    playAnotherCard: 1
  },
  {
    cardName: "Mirror Image",
    characterName: "Azzan The Mystic",
    class: "Wizard",
    countInDeck: 1,
    sheild: 3
  },
  {
    cardName: "Fireball",
    supportingText: "Each player (including you) takes 3 damage!",
    characterName: "Azzan The Mystic",
    class: "Wizard",
    countInDeck: 2,
    globalDamage: 3
  },
  {
    cardName: "Vampiric Touch",
    supportingText: "Swap your hit points with an opponent's.",
    characterName: "Azzan The Mystic",
    class: "Wizard",
    countInDeck: 2,
    hpSwap: 1
  },
  {
    cardName: "Charm",
    supportingText:
      "Take a ${sheildIcon} card that an opponent has in play—it protects you now!",
    characterName: "Azzan The Mystic",
    class: "Wizard",
    countInDeck: 2,
    shieldSteal: 1
  },
  {
    cardName: "Mighty Toss",
    supportingText:
      "Destory One %{sheildIcon} card in play then %{drawCardIcon}.",
    characterName: "Sutha The Skullcrusher",
    class: "Barbarian",
    countInDeck: 2,
    sheildBreak: 1
  },
  {
    cardName: "Whirling Axes",
    supportingText:
      "You %{healIcon} once per opponent, then %{attackIcon} each opponent.",
    characterName: "Sutha The Skullcrusher",
    class: "Barbarian",
    countInDeck: 2,
    healAndAttackPerOpponent: 1
  },
  {
    cardName: "Head Butt",
    characterName: "Sutha The Skullcrusher",
    class: "Barbarian",
    countInDeck: 2,
    attack: 1,
    playAnotherCard: 1
  },
  {
    cardName: "Brutal Punch",
    characterName: "Sutha The Skullcrusher",
    class: "Barbarian",
    countInDeck: 2,
    attack: 2
  },
  {
    cardName: "Big Ax is Best Axe",
    characterName: "Sutha The Skullcrusher",
    class: "Barbarian",
    countInDeck: 5,
    attack: 3
  },
  {
    cardName: "Spiked Shield",
    characterName: "Sutha The Skullcrusher",
    class: "Barbarian",
    countInDeck: 1,
    sheild: 2
  },
  {
    cardName: "Flex!",
    characterName: "Sutha The Skullcrusher",
    class: "Barbarian",
    countInDeck: 2,
    heal: 1,
    draw: 1
  },
  {
    cardName: "Open the Armory",
    characterName: "Sutha The Skullcrusher",
    class: "Barbarian",
    countInDeck: 2,
    draw: 2
  },
  {
    cardName: "Bag of Rats",
    characterName: "Sutha The Skullcrusher",
    class: "Barbarian",
    countInDeck: 1,
    draw: 1,
    sheild: 1
  },
  {
    cardName: "Rage!",
    characterName: "Sutha The Skullcrusher",
    class: "Barbarian",
    countInDeck: 2,
    attack: 4
  },
  {
    cardName: "Two Axes Are Better Than One",
    characterName: "Sutha The Skullcrusher",
    class: "Barbarian",
    countInDeck: 2,
    playAnotherCard: 2
  },
  {
    cardName: "Snack Time",
    characterName: "Sutha The Skullcrusher",
    class: "Barbarian",
    countInDeck: 1,
    heal: 1,
    draw: 2
  },
  {
    cardName: "Riff",
    characterName: "Sutha The Skullcrusher",
    class: "Barbarian",
    countInDeck: 1,
    sheild: 3
  },
  {
    cardName: "Raff",
    characterName: "Sutha The Skullcrusher",
    class: "Barbarian",
    countInDeck: 1,
    sheild: 3
  },
  {
    cardName: "Battle Roar",
    supportingText:
      "Each player (including you) discards their hand, then draws three cards. Then %{playAnotherCardIcon}.",
    characterName: "Sutha The Skullcrusher",
    class: "Barbarian",
    countInDeck: 2,
    playAnotherCard: 1,
    globalDiscardAndDraw: 1
  },
  {
    cardName: "Pick Pocket",
    supportingText: "Steal the top card of any player's deck and play it.",
    characterName: "Oriax The Clever",
    class: "Rogue",
    countInDeck: 2,
    stealCard: 1
  },
  {
    cardName: "Winged Serpent",
    characterName: "Oriax The Clever",
    class: "Rogue",
    countInDeck: 2,
    draw: 1,
    sheild: 1
  },
  {
    cardName: "One Thrown Dagger",
    characterName: "Oriax The Clever",
    class: "Rogue",
    countInDeck: 5,
    attack: 1,
    playAnotherCard: 1
  },
  {
    cardName: "Stolen Potion",
    characterName: "Oriax The Clever",
    class: "Rogue",
    countInDeck: 2,
    playAnotherCard: 1,
    heal: 1
  },
  {
    cardName: "Clever Disguise",
    supportingText:
      "None of your opponents' cards affect you or your %{sheildCardIcon} cards until your next turn.",
    characterName: "Oriax The Clever",
    class: "Rogue",
    countInDeck: 2,
    immunity: 1
  },
  {
    cardName: "My Little Friend",
    characterName: "Oriax The Clever",
    class: "Rogue",
    countInDeck: 1,
    sheild: 3
  },
  {
    cardName: "The Goon Squad",
    characterName: "Oriax The Clever",
    class: "Rogue",
    countInDeck: 2,
    sheild: 2
  },
  {
    cardName: "Sneak Attack",
    supportingText:
      "Destroy on %{sheildIcon} card in play, and then ${playAnotherCardIcon}.",
    characterName: "Oriax The Clever",
    class: "Rogue",
    countInDeck: 2,
    playAnotherCard: 1,
    sheildBreak: 1
  },
  {
    cardName: "All the Thrown Daggers",
    characterName: "Oriax The Clever",
    class: "Rogue",
    countInDeck: 3,
    attack: 3
  },
  {
    cardName: "Two Thrown Daggers",
    characterName: "Oriax The Clever",
    class: "Rogue",
    countInDeck: 4,
    attack: 2
  },
  {
    cardName: "Cunning Action",
    characterName: "Oriax The Clever",
    class: "Rogue",
    countInDeck: 2,
    playAnotherCard: 2
  },
  {
    cardName: "Even More Daggers",
    characterName: "Oriax The Clever",
    class: "Rogue",
    countInDeck: 1,
    heal: 1,
    draw: 2
  },
  {
    cardName: "Bearnard",
    supportingText:
      "${sheildIcon} ${sheildIcon} If in Bear Form ${healIcon}. If in Wolf Form ${attackIcon}.",
    characterName: "Jaheira The Shapeshifter",
    class: "Druid",
    countInDeck: 2,
    sheild: 2,
    formConditional: 1
  },
  {
    cardName: "Wild Rush",
    supportingText:
      "${drawCardIcon} ${drawCardIcon} If in Bear Form ${healIcon}. If in Wolf Form ${attackIcon}.",
    characterName: "Jaheira The Shapeshifter",
    class: "Druid",
    countInDeck: 2,
    draw: 2,
    formConditional: 1
  },
  {
    cardName: "The Eldest Elk",
    characterName: "Jaheira The Shapeshifter",
    class: "Druid",
    countInDeck: 1,
    sheild: 3
  },
  {
    cardName: "Poochie",
    characterName: "Jaheira The Shapeshifter",
    class: "Druid",
    countInDeck: 2,
    attack: 1,
    sheild: 1
  },
  {
    cardName: "Call Lightning",
    characterName: "Jaheira The Shapeshifter",
    class: "Druid",
    countInDeck: 3,
    attack: 3
  },
  {
    cardName: "Thorn Whip",
    characterName: "Jaheira The Shapeshifter",
    class: "Druid",
    countInDeck: 3,
    attack: 1,
    playAnotherCard: 1
  },
  {
    cardName: "Druidic Balance",
    characterName: "Jaheira The Shapeshifter",
    class: "Druid",
    countInDeck: 2,
    attack: 1,
    heal: 1,
    draw: 1
  },
  {
    cardName: "Primal Strike",
    supportingText:
      "You may make an animal noise to %{attackIcon} each opponent, then ${playAnotherCardIcon}.",
    characterName: "Jaheira The Shapeshifter",
    class: "Druid",
    countInDeck: 2,
    playAnotherCard: 1,
    animalNoise: 1
  },
  {
    cardName: "Gift of Silvanus",
    supportingText:
      "${attackIcon} ${attackIcon} If in Bear Form ${healIcon}. If in Wolf Form ${attackIcon}.",
    characterName: "Jaheira The Shapeshifter",
    class: "Druid",
    countInDeck: 2,
    attack: 2,
    formConditional: 1
  },
  {
    cardName: "Commune with Nature",
    supportingText:
      "${drawCardIcon} ${drawCardIcon}. You may play a Form card for free.",
    characterName: "Jaheira The Shapeshifter",
    class: "Druid",
    countInDeck: 2,
    draw: 2,
    form: 1
  },
  {
    cardName: "Shapeshift: Wolf Form",
    supportingText:
      "${attackIcon} ${attackIcon} You can only have 1 Form card in play at a time.",
    characterName: "Jaheira The Shapeshifter",
    class: "Druid",
    countInDeck: 3,
    attack: 2
  },
  {
    cardName: "Shapeshift: Bear Form",
    supportingText:
      "${healIcon} ${healIcon} You can only have 1 Form card in play at a time.",
    characterName: "Jaheira The Shapeshifter",
    class: "Druid",
    countInDeck: 2,
    heal: 2
  },
  {
    cardName: "Quick as a Fox",
    characterName: "Jaheira The Shapeshifter",
    class: "Druid",
    countInDeck: 2,
    playAnotherCard: 2
  },
  {
    cardName: "Twice the Smiting!",
    characterName: "Minsc & Boo",
    class: "Ranger",
    countInDeck: 3,
    attack: 2
  },
  {
    cardName: "Someone Hold My Rodent",
    characterName: "Minsc & Boo",
    class: "Ranger",
    countInDeck: 2,
    attack: 2,
    heal: 1
  },
  {
    cardName: "Krydle and Shandie",
    characterName: "Minsc & Boo",
    class: "Ranger",
    countInDeck: 1,
    sheild: 3
  },
  {
    cardName: "Wrap it Up",
    characterName: "Minsc & Boo",
    class: "Ranger",
    countInDeck: 1,
    heal: 1,
    draw: 1
  },
  {
    cardName: "Go for the Eyes BOO!!!",
    characterName: "Minsc & Boo",
    class: "Ranger",
    countInDeck: 3,
    attack: 3
  },
  {
    cardName: "Justice Waits for No One",
    characterName: "Minsc & Boo",
    class: "Ranger",
    countInDeck: 2,
    playAnotherCard: 2
  },
  {
    cardName: "Pale Priestess Nerys",
    characterName: "Minsc & Boo",
    class: "Ranger",
    countInDeck: 2,
    heal: 1,
    sheild: 1
  },
  {
    cardName: "Squeaky Wheel Gets the Kick",
    characterName: "Minsc & Boo",
    class: "Ranger",
    countInDeck: 3,
    attack: 1,
    playAnotherCard: 1
  },
  {
    cardName: "Minsc's Mighty Mount",
    characterName: "Minsc & Boo",
    class: "Ranger",
    countInDeck: 2,
    sheild: 2
  },
  {
    cardName: "Boo, What Do We Do?",
    characterName: "Minsc & Boo",
    class: "Ranger",
    countInDeck: 2,
    draw: 2
  },
  {
    cardName: "Time to Punch Evil!",
    characterName: "Minsc & Boo",
    class: "Ranger",
    countInDeck: 1,
    attack: 2,
    playAnotherCard: 1
  },
  {
    cardName: "Swapportunity",
    supportingText:
      "Each player gives their hit points total to the player on their right.",
    characterName: "Minsc & Boo",
    class: "Ranger",
    countInDeck: 2,
    globalHpSwap: 1
  },
  {
    cardName: "Scouting Outing",
    supportingText: "Draw a card from the top of each opponent's deck.",
    characterName: "Minsc & Boo",
    class: "Ranger",
    countInDeck: 2,
    globalDrawCard: 1
  },
  {
    cardName: "Favored Frienemies",
    supportingText: "Your ${attackIcon} cards deal one bonus damage this turn.",
    characterName: "Minsc & Boo",
    class: "Ranger",
    countInDeck: 2,
    playAnotherCard: 1,
    bonusDamage: 1
  }
];

export default CardData
