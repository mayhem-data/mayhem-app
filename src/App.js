import React, { Component } from 'react';
import CardTable from './CardTable'
import './App.css';

const columns = [
     {
       Header: 'Cards',
       columns: [
         {
           Header: 'Card Name',
           accessor: 'cardName',
         },
         {
           Header: 'Deck',
           accessor: 'characterName',
         },
         {
           Header: 'Card Count',
           accessor: 'countInDeck',
         }
       ],
     },
   ];

class App extends Component {
  render() {
    return (<div className="App">
      <CardTable columns={columns} data={this.props.cardData}/>
    </div>
  )}
};

export default App;
